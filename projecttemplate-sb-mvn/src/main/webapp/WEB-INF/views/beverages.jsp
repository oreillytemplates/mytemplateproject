<%@ include file="/WEB-INF/layouts/include.jsp"%>

<h1>Beverages</h1>
<div class="btn btn-primary" id="addBtn">Add Beverage</div>
<orly-table loaddataoncreate
	url="<%=request.getContextPath()%>/beverages/getBeverages/" id="table">
<orly-column name="action" label="Action">
<div slot="cell">
	<orly-icon name="edit" color="orange" data-id=\${model.id}
		id="\${`e_\${model.id}`}"></orly-icon>
	<orly-icon name="trash-2" color="tomato" data-id=\${model.id}
		id="\${`d_\${model.id}`}"></orly-icon>
</div>
</orly-column> <orly-column field="brand" label="Brand"></orly-column> <orly-column
	field="name" label="Name"></orly-column> <orly-column field="type"
	label="Type"></orly-column> <orly-column field="carbonation"
	label="Carbonation"></orly-column> <orly-column field="price"
	label="Price"></orly-column> </orly-table>

<orly-dialog class="invisible" dialogtitle="Team Member Information"
	savebtnclass="save-bev" closebtnclass="close-bev">
<div slot="body">
	<div class="container-fluid">

		<form is="orly-form" id="orlyform">
			<div class="form-group">
				<label for="brand">Brand</label> <input type="text"
					class="form-control" id="brand" name="brand">
			</div>

			<div class="form-group">
				<label for="name">Name</label> <input type="text" name="name"
					class="form-control" id="name">
			</div>

			<div class="form-group">
				<label for="type">Type</label> <input type="text" name="type"
					class="form-control" id="type">
			</div>

			<div class="row">
				<div class="col-9">
					<label for="carbonation">Carbonation</label>
				</div>
				<div class="col-3">
					<input type="checkbox" class="form-control" id="carbonation"
						name="carbonation" value=1>
				</div>

			</div>

			<div class="form-group">
				<label for="price">Price</label> <input type="number" name="price"
					class="form-control" id="price">
			</div>

			<div class="form-group d-none">
				<label for="id">id</label> <input type="text" class="form-control"
					name="id" id="id">
			</div>


		</form>

		<!-- 
		<form>
			
		</form>
 -->
	</div>
</div>
</orly-dialog>


<script>
var table;
var dialog;
var form;
var saveButton;
var saveListener = null;
var addBtn;

orly.ready.then(() => {
	// Variable Declarations
	table = orly.qid("table");
	addBtn = document.getElementById("addBtn");
	 dialog = orly.q("orly-dialog");
	 form = orly.q("form");
	 //console.log(document.getElementsByClassName("save-bev"))
	 saveButton = document.getElementsByClassName("save-bev")[0];
	
	
	// AddEventListeners
	addEventListeners();

	function addEventListeners() {
		addBtn.addEventListener("click", (e)=>{
			addBeverageEvent(e);
		});
		
		table.addEventListener("click", (e) => {
			tableEvent(e);
		});
		
		saveButton.addEventListener("click", (e)=> {
			saveRecord(e);
		});

	
	}
});
function tableEvent(e) {
	// console.log("Clicked");
	let id = e.target.id;
	
	// console.log(id.substring(0,2));
	if (id.substring(0,2) == "e_") {
		editRecord(id.substring(2));
	} else if (id.substring(0,2) == "d_") {
		deleteRecord(id.substring(2));
	}
}

function addBeverageEvent(e){
	dialog.open();
}


function editRecord(id){
	
	//load beverage from ajax
		try {
		fetch("<%=request.getContextPath()%>/beverages/get/" + id)
		.then(httpServletResponse => {
			if (httpServletResponse.ok) {
		  		let jsonPromise = httpServletResponse.json(); // object from controller
		  		return jsonPromise;
		  	} else {
		  		throw new Error(getError(httpServletResponse));
		  	}
		}).then((Response) => {						
			if (Response != null && typeof Response != "undefined") {
				let messageType = "info";
				let message = Response.message;
				messageType = ((Response.messageType.length > 0) ? Response.messageType : messageType); 
				orly.qid("alerts").createAlert({type:messageType, duration:"3000", msg:message});
				populateEditDialogWithResponse(Response);
			} else {
				orly.qid("alerts").createAlert({type:"danger", duration:"3000", msg:"Unknown Response From The Server"});
			}
		}).catch(function(error) {
			console.log('There was a problem with your fetch operation: ', error.message);
		});
	} catch (err) {
		alert("Yo stuff is broken!");
	}
	
	 dialog.open();

	/*  saveButton = document.getElementsByClassName("save-bev")[0];
	 if(saveListener != null){
		 saveButton.removeEventListener(saveListener);
		 saveListener = null;
	 }
			saveListener = saveButton.addEventListener("click", (e)=> {
				saveRecord();
		});
				 */
		
	
}


function saveRecord(){
	const id = document.getElementById("id").value;
	const brand = document.getElementById("brand").value;
	const name = document.getElementById("name").value;
	const carbonation = document.getElementById("carbonation").value;
	const price = document.getElementById("price").value;
	const type = document.getElementById("type").value;
	const carbonationElem = document.getElementById("carbonation");
	const beverage = {};
	beverage.id = id;
	beverage.brand = brand;
	beverage.name = name;
	beverage.carbonation = carbonationElem.checked ? true : false;
	beverage.price = price;
	beverage.type = type;
	
	
	console.log("saving bev: ");
	console.log(beverage);
	
	
	//ajax
		try {
			fetch("<c:url value='/beverages/save' />", {
		        method: "POST",
		        body: JSON.stringify(beverage),
		        headers: {
		            "Content-Type": "application/json"
		        }
			})
		.then(httpServletResponse => {
			if (httpServletResponse.ok) {
		  		let jsonPromise = httpServletResponse.json(); // object from controller
		  		return jsonPromise;
		  	} else {
		  		throw new Error(getError(httpServletResponse));
		  	}
		}).then((Response) => {						
			if (Response != null && typeof Response != "undefined") {
				let messageType = "info";
				let message = Response.message;
				messageType = ((Response.messageType.length > 0) ? Response.messageType : messageType); 
				orly.qid("alerts").createAlert({type:messageType, duration:"3000", msg:message});
				populateTableWithResponse(Response);
				form.reset();
				dialog.close();
			} else {
				orly.qid("alerts").createAlert({type:"danger", duration:"3000", msg:"Unknown Response From The Server"});
			}
		}).catch(function(error) {
			console.log('There was a problem with your fetch operation: ', error.message);
		});
	} catch (err) {
		alert("Yo stuff is broken!");
	}
}

function deleteRecord(id) {
	try {
		fetch("<%=request.getContextPath()%>/beverages/delete/" + id)
		.then(httpServletResponse => {
			if (httpServletResponse.ok) {
		  		let jsonPromise = httpServletResponse.json(); // object from controller
		  		return jsonPromise;
		  	} else {
		  		throw new Error(getError(httpServletResponse));
		  	}
		}).then((Response) => {						
			if (Response != null && typeof Response != "undefined") {
				let messageType = "info";
				let message = Response.message;
				messageType = ((Response.messageType.length > 0) ? Response.messageType : messageType); 
				orly.qid("alerts").createAlert({type:messageType, duration:"3000", msg:message});
				populateTableWithResponse(Response);
			} else {
				orly.qid("alerts").createAlert({type:"danger", duration:"3000", msg:"Unknown Response From The Server"});
			}
		}).catch(function(error) {
			console.log('There was a problem with your fetch operation: ', error.message);
		});
	} catch (err) {
		alert("Yo stuff is broken!");
	}
	
}

function populateEditDialogWithResponse(response){
	if(response.beverage == null){
		return;
	}
	const bev = response.beverage;

	form.values = bev;
}

function populateTableWithResponse(Response) {
	try {
		let beveragesList = Response.beveragesList;
		
		if (beveragesList != null && typeof beveragesList != "undefined") {
			table.data = beveragesList;
		} else {
			console.log("Unable to populate table with new beverage list");
		}	
	} catch (e) {
		console.log("Unable to populate table with new beverage list");	
	}
}
</script>