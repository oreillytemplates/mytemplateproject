package com.oreillyauto.service;

import java.util.List;

import com.oreillyauto.domain.Beverages;

public interface BeveragesService {
    public List<Beverages> getBeverages();

    public void deleteById(Integer id);

    public Beverages getBeverageById(Integer id);

    public void saveBeverage(Beverages beverage);
}
