package com.oreillyauto.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oreillyauto.dao.BeveragesRepository;
import com.oreillyauto.domain.Beverages;
import com.oreillyauto.service.BeveragesService;

@Service("beveragesService")
public class BeveragesServiceImpl implements BeveragesService {
    @Autowired
    BeveragesRepository bevRepo;

    @Override
    public List<Beverages> getBeverages() {
        return (List<Beverages>) bevRepo.findAll();
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        bevRepo.deleteById(id);
    }

    @Override
    public Beverages getBeverageById(Integer id) {
        Optional<Beverages> beverage = bevRepo.findById(id);

        return beverage.isPresent() ? beverage.get() : null;
    }

    @Override
    @Transactional
    public void saveBeverage(Beverages beverage) {
     bevRepo.save(beverage);
        
    }

}
