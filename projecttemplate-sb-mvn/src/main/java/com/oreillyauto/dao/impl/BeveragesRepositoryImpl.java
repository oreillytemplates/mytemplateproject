package com.oreillyauto.dao.impl;

import java.sql.Timestamp;

import javax.persistence.Query;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import com.oreillyauto.dao.custom.BeveragesRepositoryCustom;
import com.oreillyauto.domain.Beverages;
import com.oreillyauto.domain.QBeverages;

@Repository
public class BeveragesRepositoryImpl extends QuerydslRepositorySupport implements BeveragesRepositoryCustom {
	
    QBeverages beveragesTable = QBeverages.beverages;
    
	public BeveragesRepositoryImpl() {
		super(Beverages.class);
	}

	

}
