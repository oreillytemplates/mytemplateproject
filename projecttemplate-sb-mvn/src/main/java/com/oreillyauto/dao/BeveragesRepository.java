package com.oreillyauto.dao;

import org.springframework.data.repository.CrudRepository;

import com.oreillyauto.dao.custom.BeveragesRepositoryCustom;
import com.oreillyauto.domain.Beverages;

public interface BeveragesRepository extends CrudRepository<Beverages, Integer>, BeveragesRepositoryCustom {
	// Spring Data abstract methods go here

}
