package com.oreillyauto.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Response implements Serializable {
    private static final long serialVersionUID = 7092938701510329645L;

    public Response() {}

    private String message;
    private String messageType;
    private List<Beverages> beveragesList = new ArrayList<Beverages>();
    private Beverages beverage;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public List<Beverages> getBeveragesList() {
        return beveragesList;
    }

    public void setBeveragesList(List<Beverages> beveragesList) {
        this.beveragesList = beveragesList;
    }

    public Beverages getBeverage() {
        return beverage;
    }

    public void setBeverage(Beverages beverage) {
        this.beverage = beverage;
    }

}
