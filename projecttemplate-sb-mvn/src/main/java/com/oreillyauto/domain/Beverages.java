package com.oreillyauto.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="beverages")
public class Beverages implements Serializable{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    public Beverages() {
        super();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", columnDefinition = "INTEGER")
    private Integer id;
    
    @Column(name = "BRAND", columnDefinition = "VARCHAR(250)")
    private String brand;
    
    @Column(name = "NAME", columnDefinition = "VARCHAR(250)")
    private String name;
    
    @Column(name = "TYPE", columnDefinition = "VARCHAR(250)")
    private String type;
    
    @Column(name = "CARBONATION", columnDefinition = "BOOLEAN")
    private Boolean carbonation;
    
    @Column(name = "PRICE", columnDefinition = "DECIMAL(10,2)")
    private BigDecimal price;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getCarbonation() {
        return carbonation;
    }

    public void setCarbonation(Boolean carbonation) {
        this.carbonation = carbonation;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Beverages [id=" + id + ", brand=" + brand + ", name=" + name + ", type=" + type + ", carbonation=" + carbonation
                + ", price=" + price + "]";
    }

}
