package com.oreillyauto.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.oreillyauto.domain.Beverages;
import com.oreillyauto.domain.Response;
import com.oreillyauto.service.BeveragesService;
import com.oreillyauto.util.Helper;

@Controller
public class BeveragesController {
    @Autowired
    BeveragesService beveragesService;

    @GetMapping(value = "/beverages")
    public String getDemBeverages(Model model) {
        return "beverages";
    }

    @ResponseBody
    @GetMapping(value = "/beverages/getBeverages")
    public List<Beverages> getAllDemBeverages() {
        return beveragesService.getBeverages();
    }

    @ResponseBody
    @GetMapping(value = "/beverages/get/{id}")
    public Response getDatBeverage(@PathVariable String id) {
        Response response = new Response();

        try {
            Integer realID = Helper.isInteger(id) ? Integer.parseInt(id) : 0;
            if (id != null) {

                response.setMessage("Editing Beverage");
                response.setMessageType("success");
                response.setBeverage(beveragesService.getBeverageById(realID));
            } else {
                response.setMessage("Sorry, unable to find that beverage.");
                response.setMessageType("danger");
            }

            return response;
        }
        catch (Exception e) {
            response.setMessage(Helper.getError(e));
            response.setMessageType("danger");
            return response;
        }

    }

    @ResponseBody
    @GetMapping(value = "/beverages/delete/{id}")
    public Response deleteBeverage(@PathVariable Integer id) {

        Response response = new Response();

        try {

            if (id != null) {
                beveragesService.deleteById(id);
                response.setMessage("Beverage Successfully Deleted");
                response.setMessageType("success");
                response.setBeveragesList(beveragesService.getBeverages());
            } else {
                response.setMessage("Sorry, unable to find that beverage.");
                response.setMessageType("danger");
            }

            return response;
        }
        catch (Exception e) {
            response.setMessage(Helper.getError(e));
            response.setMessageType("danger");
            return response;
        }
    }
    
    
    @ResponseBody
    @PostMapping(value = "/beverages/save")
    public Response saveBeverage(@RequestBody Beverages beverage) {

        Response response = new Response();

        try {

            if (beverage != null) {
                beveragesService.saveBeverage(beverage);
                response.setMessage("Beverage Successfully Saved");
                response.setMessageType("success");
                response.setBeveragesList(beveragesService.getBeverages());
            } else {
                response.setMessage("Sorry, unable to find that beverage.");
                response.setMessageType("danger");
            }

            return response;
        }
        catch (Exception e) {
            response.setMessage(Helper.getError(e));
            response.setMessageType("danger");
            return response;
        }
    }
    
}
